package com.year.leap.exercise.leapyearcheck

import android.support.v7.app.AppCompatActivity
import android.os.Bundle

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }

    override fun onStart() {
        super.onStart()
        checkLeapYear()
    }
}

fun checkLeapYear() {
    println("Is 2000 leap ${checkLeap(2000)}")
    println("Is 1990 leap ${checkLeap(1990)}")
    println("Is 1992 leap ${checkLeap(1992)}")
    println("Is 1994 leap ${checkLeap(1994)}")
    println("Is 1993 leap ${checkLeap(1993)}")
}

fun checkLeap(year: Int) = when {
    (year % 400 == 0 || (year % 4 == 0 && year % 100 != 0)) -> true
    else -> false
}